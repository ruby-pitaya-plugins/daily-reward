Given(/^[Cc]lient call route ["'](.+)["']$/) do |route|
  @app_helper.request(route)
end

Given(/^[Cc]lient call route ["'](.+)["'] with param[s]*[:]*$/) do |route, table|
  params = table.hashes.first.symbolize_keys
  @app_helper.request(route, params)
end

Given(/^[Cc]lient call route ["'](.+)["'] with json param[s]*[:]*$/) do |route, json|
  params = JSON.parse(json, symbolize_names: true)
  @app_helper.request(route, params)
end

Given(/^[Ss]erver should response ["'](.+)["'] as ["'](.+)["']$/) do |response_key, expected_value|
  response_value = @app_helper.response.dig(*response_key.split('.').map(&:to_sym))

  expect(response_value.to_s).to eq(expected_value)
end

Given(/^[Ss]erver should response the following json[:]*$/) do |expected_json|
  expected_json = JSON.generate(JSON.parse(expected_json.strip))
  response_json = JSON.generate(@app_helper.response)

  puts JSON.pretty_generate(@app_helper.response) if expected_json != response_json

  expect(response_json).to eq(expected_json)
end

Given(/^[Ss]etup key ["'](.+)["'] is ["'](.+)["']$/) do |key, value|
  @app_helper.add_setup(key, value)
end

Given(/^[Cc]onfig key ["'](.+)["'] is ["'](.+)["']$/) do |key, value|
  @app_helper.add_config(key, value)
end

Given(/^[Cc]onfig is the following json[:]*$/) do |config_json|
  config = JSON.parse(config_json)
  @app_helper.set_config(config)
end

Given(/^[Pp]rint server response$/) do 
  puts "response: #{@app_helper.response}"
end

Given(/^(?:[Nn]ow\s)?[Tt]oday is ["'](.+)["']$/) do |date_text|
  time_now = date_text.to_time

  Time.stub(:now).and_return(time_now)
end

Given(/^[Tt]he [Uu]ser ["'](.+)["'] is authenticated$/) do |user_id|
  @app_helper.authenticate(user_id)
end

Given(/^[Tt]he following [Uu]ser[s]*[:]*$/) do |table|
  table.hashes.each do |table_hash|
    User.create(id: table_hash[:user_id])
  end
end

Before do
  ActiveRecord::Base.descendants.each { |c| c.delete_all unless c == ActiveRecord::SchemaMigration }
  RubyPitaya::AppSpecHelper.update_before_each
  @app_helper = RubyPitaya::AppSpecHelperClass.new('cucumber')
end

# After do
# end