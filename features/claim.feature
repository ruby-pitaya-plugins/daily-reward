Feature: Claim daily rewards

  As a player I want to claim rewards every day

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And the user '00000000-0000-0000-0000-000000000001' is authenticated
    And config is the following json:
      """
      {
        "plugin/daily_reward/codes": [
          "some-daily-reward"
        ],
        "plugin/daily_reward/some-daily-reward": {
          "code": "some-daily-reward",
          "title": "3 days",
          "rewards": [
            {
              "code": "gold",
              "amount": 10
            },
            {
              "code": "gold",
              "amount": 20
            },
            {
              "code": "gold",
              "amount": 50
            }
          ]
        }
      }
      """

  Scenario: Claim first daily reward
    Given client call route 'rubypitaya.dailyRewardHandler.claim'
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "claimedDailyRewards": [
            {
              "code": "some-daily-reward",
              "title": "3 days",
              "rewards": [
                {
                  "code": "gold",
                  "amount": 10,
                  "status": "claimed-now"
                },
                {
                  "code": "gold",
                  "amount": 20,
                  "status": "not-claimed"
                },
                {
                  "code": "gold",
                  "amount": 50,
                  "status": "not-claimed"
                }
              ]
            }
          ]
        }
      }
      """

  Scenario: Don't claim reward if call claim at the same day
    Given client call route 'rubypitaya.dailyRewardHandler.claim'
    When client call route 'rubypitaya.dailyRewardHandler.claim'
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "claimedDailyRewards": []
        }
      }
      """

  Scenario: Claim reward after wait a day for claim again
    Given today is '2020-12-01 00:00:00'
    And client call route 'rubypitaya.dailyRewardHandler.claim'
    And now today is '2020-12-02 00:00:00'
    When client call route 'rubypitaya.dailyRewardHandler.claim'
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "claimedDailyRewards": [
            {
              "code": "some-daily-reward",
              "title": "3 days",
              "rewards": [
                {
                  "code": "gold",
                  "amount": 10,
                  "status": "claimed"
                },
                {
                  "code": "gold",
                  "amount": 20,
                  "status": "claimed-now"
                },
                {
                  "code": "gold",
                  "amount": 50,
                  "status": "not-claimed"
                }
              ]
            }
          ]
        }
      }
      """

  Scenario: Don't claim reward if the player has already claimed all rewards
    Given today is '2020-12-01 00:00:00'
    And client call route 'rubypitaya.dailyRewardHandler.claim'
    And now today is '2020-12-02 00:00:00'
    And client call route 'rubypitaya.dailyRewardHandler.claim'
    And now today is '2020-12-03 00:00:00'
    And client call route 'rubypitaya.dailyRewardHandler.claim'
    And now today is '2020-12-04 00:00:00'
    When client call route 'rubypitaya.dailyRewardHandler.claim'
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "claimedDailyRewards": []
        }
      }
      """

  Scenario: Don't claim reward if the config file is not listed on codes.json
    Given config is the following json:
      """
      {
        "plugin/daily_reward/codes": [
          "non-existent-daily-reward"
        ],
        "plugin/daily_reward/some-daily-reward": {
          "code": "some-daily-reward",
          "title": "3 days",
          "rewards": [
            {
              "code": "gold",
              "amount": 10
            }
          ]
        }
      }
      """
    When client call route 'rubypitaya.dailyRewardHandler.claim'
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "claimedDailyRewards": []
        }
      }
      """