require 'active_record'

class CreatePlayerDailyRewardMigration < ActiveRecord::Migration[5.1]

  enable_extension 'pgcrypto'

  def change
    create_table :player_daily_rewards, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, foreing_key: true

      t.string  :code,  null: false
      t.integer :reward_index, default: 0
      t.datetime :claimed_at, default: DateTime.new

      t.timestamps null: false
    end

    add_index :player_daily_rewards, [:code, :id], unique: true
  end
end
