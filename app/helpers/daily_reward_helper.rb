module DailyReward

  class DailyRewardHelper

    def initialize(config)
      @config = config
    end

    def get_daily_reward_codes
      codes = @config['plugin/daily_reward/codes']
      configs = codes.map {|code| @config["plugin/daily_reward/#{code}"] }
      configs.compact.pluck(:code)
    end

    def get_player_daily_rewards(user_id)
      codes = get_daily_reward_codes()

      player_daily_rewards = PlayerDailyReward.where(user_id: user_id, code: codes).to_a

      new_codes = codes - player_daily_rewards.map(&:code)
      new_codes.each do |code|
        player_daily_rewards << PlayerDailyReward.new(
          user_id: user_id,
          code: code,
          claimed_at: DateTime.new,
        )
      end

      player_daily_rewards
    end
  end
end
