require 'active_record'

module DailyReward

  class PlayerDailyReward < ActiveRecord::Base

    belongs_to :user

    validates :code, uniqueness: { scope: :user_id }

    validates_presence_of :code, :reward_index, :user

    def to_hash
      {
        code: code,
        reward_index: reward_index,
        claimed_at: claimed_at,
      }
    end
  end
end