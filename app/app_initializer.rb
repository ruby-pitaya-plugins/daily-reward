module DailyReward

  class AppInitializer < RubyPitaya::InitializerBase

    # method:     run
    # parameter:  initializer_content
    # attributes:
    #  - services
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/service_holder.rb
    #
    #  - config
    #    - class: RubyPitaya::Config
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/config.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - setup
    #    - class: RubyPitaya::Setup
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/setup.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - log
    #    - class: Logger
    #    - link: https://ruby-doc.org/stdlib-2.6.4/libdoc/logger/rdoc/Logger.html
    #    - methods:
    #      - info
    #        - log information
    #
    # services:
    #  - redis
    #    - link:
    #      - https://github.com/redis/redis-rb/
    #      - https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/app/services/redis_service.rb
    #

    def run(initializer_content)
      config = initializer_content.config

      daily_reward_bll = DailyRewardBLL.new
      daily_reward_helper = DailyRewardHelper.new(config)

      DailyRewardHandler.objects.add(:bll, daily_reward_bll)
      DailyRewardHandler.objects.add(:helper, daily_reward_helper)
    end

    def self.path
      __FILE__
    end
  end
end
