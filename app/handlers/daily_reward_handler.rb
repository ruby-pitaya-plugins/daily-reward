module DailyReward

  class DailyRewardHandler < RubyPitaya::HandlerBase

    STATUS_CLAIMED = 'claimed'
    STATUS_CLAIMED_NOW = 'claimed-now'
    STATUS_NOT_CLAIMED = 'not-claimed'

    def claim
      user_id = @session.uid

      player_daily_rewards = helper.get_player_daily_rewards(user_id)
      player_daily_rewards = get_claimable_daily_rewards(player_daily_rewards)

      claimed_daily_reward_infos = []

      if !player_daily_rewards.empty?
        ActiveRecord::Base.transaction do
          bll.before_claim_daily_rewards(user_id)
          claimed_daily_reward_infos = claim_player_daily_rewards(user_id, player_daily_rewards)
          bll.after_claim_daily_rewards(user_id)
        end
      end

      response = {
        code: StatusCodes::CODE_OK,
        data: {
          claimedDailyRewards: claimed_daily_reward_infos,
        }
      }
    end

    private

    def bll
      @objects[:bll]
    end

    def helper
      @objects[:helper]
    end

    def get_claimable_daily_rewards(player_daily_rewards)
      timestamp_now = Time.now.utc.to_i

      player_daily_rewards.select do |player_daily_reward|
        daily_reward = @config["plugin/daily_reward/#{player_daily_reward.code}"]
        
        has_reward_to_claim = player_daily_reward.reward_index < daily_reward[:rewards].size
        is_time_to_claim = timestamp_now - player_daily_reward.claimed_at.utc.beginning_of_day.to_i >= (3600 * 24)

        has_reward_to_claim && is_time_to_claim
      end
    end

    def claim_player_daily_rewards(user_id, player_daily_rewards)
      player_daily_rewards.map {|player_daily_reward| claim_player_daily_reward(user_id, player_daily_reward)}
    end

    def claim_player_daily_reward(user_id, player_daily_reward)
      daily_reward = @config["plugin/daily_reward/#{player_daily_reward.code}"]

      claimed_daily_reward_info = {
        code: daily_reward[:code],
        title: daily_reward[:title],
        rewards: [],
      }
      
      daily_reward[:rewards].each_with_index do |reward, i|
        reward_info = reward.clone
        reward_status = get_reward_status(i, player_daily_reward.reward_index)

        if reward_status == STATUS_CLAIMED_NOW
          reward_info[:dailyRewardCode] = daily_reward[:code]
          reward_info = bll.claim_reward(user_id, reward_info)
          reward_info.delete(:dailyRewardCode)
        end

        reward_info[:status] = reward_status
        claimed_daily_reward_info[:rewards] << reward_info
      end

      player_daily_reward.reward_index += 1
      player_daily_reward.claimed_at = Time.now.utc
      player_daily_reward.save!

      claimed_daily_reward_info
    end

    def get_reward_status(reward_index, player_reward_index)
        return STATUS_CLAIMED_NOW if reward_index == player_reward_index
        return STATUS_CLAIMED if reward_index < player_reward_index
        return STATUS_NOT_CLAIMED
    end
  end
end
