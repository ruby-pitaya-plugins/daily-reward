module DailyReward

  class DailyRewardBLL

    # parameter:  reward = {
    #               code:            [string],
    #               amount:          [int],
    #               dailyRewardCode: [string],
    #               [others fields on config file]
    #             }

    # parameter:  user_id = [string]

    def before_claim_daily_rewards(user_id)
    end

    def after_claim_daily_rewards(user_id)
    end

    def claim_reward(user_id, reward)
      reward
    end
  end
end